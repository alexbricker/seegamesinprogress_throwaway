import { Component, OnInit } from '@angular/core';
import { Game } from '../modules/game';
import { GameServices } from '../modules/game.services';
import { EMPTY_SOURCE_SPAN } from '@angular/compiler';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    games: Game[] = [];

    constructor(private gameService: GameServices) { }

    ngOnInit() {
        this.getGames();
    }

    getGames(): void {
        this.gameService.getGames()
          .subscribe(games => this.games = games.slice(0, 5));
    }
}