import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { GameServices } from './modules/game.services';
import { MessageService } from './messages/message.service';
import { MessagesComponent } from './messages/messages.component';
import { TempComponent } from './temp/temp.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    TempComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ GameServices, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
