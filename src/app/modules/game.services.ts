import { Observable, of } from 'rxjs';
import { Game } from './game';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { MessageService } from '../messages/message.service';

@Injectable()
export class GameServices {

    private gamesUrl = 'https://localhost:44376/games';

    constructor(
        private http: HttpClient,
    private messageService: MessageService) { }

    getGames (): Observable<Game[]> {
        return this.http.get<Game[]>(this.gamesUrl)
        .pipe(
            tap(games => this.log('fetched games')),
            catchError(this.handleError('getGames', []))
        );
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            console.error(error);

            this.log(`${operation} failed: ${error.message}`);

            return of(result as T);
        };
    }

    private log(message: string) {
        this.messageService.noGames('MessageService' + message);
    }
}