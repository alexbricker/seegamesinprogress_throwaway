import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
    message: string[] = [];

    noGames(message: string) {
        this.message.push("no games found");
    }

    clear() {
        this.message = [];
    }
}