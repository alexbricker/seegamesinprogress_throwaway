import { MessageService } from "../messages/message.service";

describe('MessageService', () => {
    let service: MessageService

    beforeEach(() => {
        service = new MessageService();
    })

    it('should have no messagesto start', () => {
        let result = service.message.length;

        expect(result).toBe(0);
    })

    it('should add no games found message when noGames is called', () => {
        service.noGames('no games found');

        let result = service.message.length;

        expect(result).toBe(1);
    })

    it('should remove all messages when clear is called', () => {
        service.noGames('no games found');

        service.clear();
        let result = service.message.length;

        expect(result).toBe(0);
    })
})