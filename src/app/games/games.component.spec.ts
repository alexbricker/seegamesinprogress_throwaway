import { GamesComponent } from "./games.component";
import { of } from "rxjs";

describe('GamesComponent', () => {
    let component: GamesComponent;
    let GAMES;
    let mockGamesServices;

    beforeEach(() => {
        GAMES = [
            {id:1, name: 'Game One', ended: false},
            {id:2, name: 'Game Two', ended: false},
            {id:3, name: 'Game Three', ended: false}
        ]

        mockGamesServices = jasmine.createSpyObj(['getGames'])

        component = new GamesComponent(mockGamesServices);
    })

    describe('getGames', () => {
        
        it('should send request to GamesServices and recieve list of playable games', () => {
            mockGamesServices.getGames.and.returnValue(of());
            component.games = GAMES;

            component.getGames();
            let result = component.games.length;

            expect(component.games.length).toBe(3);
        })
    })
})