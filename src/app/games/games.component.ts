import { Component, OnInit } from '@angular/core';

import { GameServices } from '../modules/game.services';
import { Game } from '../modules/game';

@Component({
    selector: 'app-games',
    templateUrl: './games.component.html',
    styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
    games: Game[];

    constructor(private gameService: GameServices) { }

    ngOnInit() {
        this.getGames();
    }

    getGames(): void {
        this.gameService.getGames()
        .subscribe(games => this.games = games);
    }
}